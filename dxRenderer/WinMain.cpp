#include "stdafx.h"
#include "App.h"
#include "ExceptionHandler.h"

int WINAPI WinMain(
	_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ LPSTR lpszCmdLine, 
	_In_ int nCmdShow)
{
	try {
		return App{}.Init();	
	}
	catch (const ExceptionHandler& e) {
		MessageBox(nullptr, LPCWSTR(e.what()), LPCWSTR(e.GetType()), MB_OK | MB_ICONEXCLAMATION);
	}
	catch (const std::exception & e) {
		MessageBox(nullptr, LPCWSTR(e.what()), L"Standard Exception", MB_OK | MB_ICONEXCLAMATION);
	}
	catch (...) {
		MessageBox(nullptr, L"No details available", L"Unknown Exception", MB_OK | MB_ICONEXCLAMATION);
	}

	return -1;
}