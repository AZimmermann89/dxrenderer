#pragma once

using Microsoft::WRL::ComPtr;

class RenderCore
{
	static const UINT FRAME_COUNT = 2;

	struct Vertex
	{
		DirectX::XMFLOAT3 position;
		DirectX::XMFLOAT4 color;
	};

public:
	RenderCore(HWND hWnd);
	~RenderCore();
	RenderCore(const RenderCore&) = delete;
	RenderCore& operator=(const RenderCore&) = delete;
	void EndFrame();
protected:
	void GetHardwareAdapter(IDXGIFactory2* pFactory, IDXGIAdapter1** ppAdapter);
private:
	ComPtr<ID3D12Device> pDevice;
	ComPtr<ID3D12CommandQueue> pCommandQueue;
	ComPtr<IDXGISwapChain3> pSwapChain;
	ComPtr<ID3D12DescriptorHeap> pRtvHeap;
	ComPtr<ID3D12Resource> pRenderTargets[FRAME_COUNT];
	ComPtr<ID3D12CommandAllocator> pCommandAllocator;
	ComPtr<ID3D12RootSignature> pRootSignature;
	ComPtr<ID3D12PipelineState> pPipelineState;
	ComPtr<ID3D12GraphicsCommandList> pCommandList;

	ComPtr<ID3D12Resource> pVertexBuffer;
	D3D12_VERTEX_BUFFER_VIEW pVertexBufferView;
	
	UINT frameIndex;
	UINT rtvDescriptorSize;

	HANDLE pFenceEvent;
	ComPtr<ID3D12Fence> pFence;
	UINT64 pFenceValue;

	std::wstring assetsPath;

	void LoadPipeline(HWND hWnd);
	void LoadAssets();
	std::wstring GetAssetFullPath(LPCWSTR assetName);
	void WaitForPreviousFrame();
};