#include "stdafx.h"
#include "App.h"

App::App()
	: wnd( 800, 600, L"DxRenderer Title")
{}

int App::Init()
{
	while (true)
	{
		if (const auto ecode = Window::ProcessMessage())
		{
			return *ecode;
		}
		DrawFrame();
	}
}

void App::DrawFrame()
{
}
